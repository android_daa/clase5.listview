package com.example.duoc.lista20;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class ListandoActivity extends AppCompatActivity {

    private ListView lvJuegos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listando);

        lvJuegos = (ListView)findViewById(R.id.lvJuegos);
        final String[] datos = new String[50];

        for(int x = 0 ; x < 50 ; x++){
            datos[x] = "Elemento " + x;
        }

        ArrayAdapter<String> adaptador =
                new ArrayAdapter<String>(this,
                        android.R.layout.simple_list_item_1, datos);

        lvJuegos.setAdapter(adaptador);

    }
}
