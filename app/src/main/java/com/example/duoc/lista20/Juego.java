package com.example.duoc.lista20;

/**
 * Created by DUOC on 08-04-2017.
 */

public class Juego {
    private String nombre;
    private String categoria;
    private String plataforma;
    private String urlImagen;

    public Juego(String nombre, String categoria, String plataforma, String urlImagen) {
        this.nombre = nombre;
        this.categoria = categoria;
        this.plataforma = plataforma;
        this.urlImagen = urlImagen;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getPlataforma() {
        return plataforma;
    }

    public void setPlataforma(String plataforma) {
        this.plataforma = plataforma;
    }

    public String getUrlImagen() {
        return urlImagen;
    }

    public void setUrlImagen(String urlImagen) {
        this.urlImagen = urlImagen;
    }
}
