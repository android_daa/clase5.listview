package com.example.duoc.lista20;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by DUOC on 08-04-2017.
 */

public class JuegoAdapter extends ArrayAdapter<Juego> {

    private ArrayList<Juego> dataSource;

    public JuegoAdapter(Context context, ArrayList<Juego> dataSource){
        super(context, R.layout.item_juego, dataSource);
        this.dataSource = dataSource;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View item = inflater.inflate(R.layout.item_juego, null);

        TextView tvTitulo = (TextView)item.findViewById(R.id.tvTitulo);
        tvTitulo.setText(dataSource.get(position).getNombre());

        TextView tvCategoria = (TextView)item.findViewById(R.id.tvCategoria);
        tvCategoria.setText(dataSource.get(position).getCategoria());

        TextView tvPlataforma = (TextView)item.findViewById(R.id.tvPlataforma);
        tvPlataforma.setText(dataSource.get(position).getPlataforma());

        ImageView ivPreview = (ImageView)item.findViewById(R.id.ivPreview);
        Picasso.with(getContext())
                .load(dataSource.get(position).getUrlImagen())
                .into(ivPreview);


        return(item);
    }
}
