package com.example.duoc.lista20;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class ListaCustomizadaActivity extends AppCompatActivity {

    private ListView lvJuegosCustom;
    private ArrayList<Juego> dataSource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_customizada);

        lvJuegosCustom = (ListView)findViewById(R.id.lvJuegosCustom);
        JuegoAdapter adaptador = new JuegoAdapter(this, getDataSource());
        lvJuegosCustom.setAdapter(adaptador);

        lvJuegosCustom.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(ListaCustomizadaActivity.this,
                        getDataSource().get(position).getNombre(), Toast.LENGTH_LONG).show();
            }
        });

    }


    private ArrayList<Juego> getDataSource(){
        if(dataSource == null) {
            dataSource = new ArrayList<>();

            for (int x = 0; x < 50; x++) {
                Juego aux = new Juego("Juego" + x, "shooter", "PC|PS4|XBOX ONE", "http://www.fastweb.it/var/storage_feeds/CMS/articoli/c99/c99ce8410c45106d566b97a91888d4b4/orig.jpg");
                dataSource.add(aux);
            }
        }
        return dataSource;
    }
}
